import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FlowLayoutDemo {
	public static void main(String[] args) {
		new LayoutFrame("FlowLayout", new DemoFlowLayout());
	}
}

class LayoutFrame extends JFrame {
	public LayoutFrame(String title, JPanel contents) {
		super(title);
		this.setContentPane(contents);
		this.setSize(300, 230);
		this.setMinimumSize(new Dimension(150,  100));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
}


class DemoFlowLayout extends JPanel {
	public DemoFlowLayout() {
		this.setLayout(new FlowLayout());
		this.add(new JButton("One"));
		this.add(new JButton("Two"));
		this.add(new JButton("Three"));
		this.add(new JButton("Four"));
	}
}
